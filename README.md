# Mass file rename via vim (or editor of choice)
I happened to need this and like the nerd i am desided to write my own instead of using someone elses.

## TODO
1. Add confirmation if a move would overwrite another file.
2. Add options (-f to overwrite files)
3. Use the $EDITOR env variable if it exists.
4. Add deletion of files
