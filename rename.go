// Mass rename files via a text editor.
package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"runtime"
	"strings"
)

func main() {
	if err := run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}

func run() (err error) {
	defer recoverErrors(&err)

	files, err := ioutil.ReadDir(".")
	mustNot(err)

	tmpFile, err := ioutil.TempFile("", "rename-*")
	mustNot(err)
	defer func() {
		err := os.Remove(tmpFile.Name())
		mustNot(err)
	}()

	for _, file := range files {
		_, err := tmpFile.WriteString(file.Name() + "\n")
		mustNot(err)
	}
	err = tmpFile.Close()
	mustNot(err)

	// let them edit the file names.
	nvim(tmpFile.Name())

	// now we read in the new files.
	data, err := ioutil.ReadFile(tmpFile.Name())
	mustNot(err)

	newFiles := strings.Split(string(data), "\n")

	// now for the tricky bit...
	for i, newFile := range newFiles {
		if len(files) < i {
			return fmt.Errorf("len(files) is %d, but the new file count is %d", len(files), i)
		}

		if newFile == files[i].Name() {
			continue
		}

		fmt.Println(files[i].Name() + " -> " + newFile)
		err := os.Rename(files[i].Name(), newFile)
		mustNot(err)
	}

	return
}

func mustNot(err error) {
	if err != nil {
		panic(err)
	}
}

// must be ran inside defer
func recoverErrors(errp *error) {
	e := recover()
	if _, ok := e.(runtime.Error); ok {
		return
	}

	if err, ok := e.(error); ok {
		*errp = err
	}
}

func nvim(args ...string) error {
	cmd := exec.Command("nvim", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	return cmd.Run()
}
